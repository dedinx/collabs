<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Illuminate\Support\Facades\File;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function user()
    {
    	$id_user = Auth::user()->id;
    	$user = User::find($id_user);
    	return view('profile', ['user'=>$user]);
    }

    public function update(Request $request)
    {


		$user = User::find($request->id);

    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->alamat = $request->alamat;
    	if ($request->photo_profile) {
            File::delete(public_path('profile/' . $user->photo_profile));

            /*request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);*/

            $nama_file = time() . '.' . request()->photo_profile->getClientOriginalName();
            request()->photo_profile->move(public_path('profile'), $nama_file);
        }
        else {
            $nama_file = $user->photo_profile;
        }
    	$user->photo_profile= $nama_file;
        $user->update();
        return redirect('/user')->with('success', 'Data berhasil diupdate');
    }
}
