<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Post;
class PostController extends Controller
{
    //

    public function __construct(){
        $this->middleware('auth');
    }

    public function create(){
        $posts2 = DB::table('posts')
                            ->where('users_id','=',Auth::id())
                            ->get();
        // dd($posts);
        $posts = $posts2->reverse();
        return view('posts.create',compact('posts'))->with('success','Status Berhasil Dibuat!!');
    }

    public function store(Request $request){
    

        $post = Post::create([
            "konten_posting" => $request["judul"],
            "users_id" => Auth::id()
        ]);
        
        return redirect('/posts/create');

    }

    public function index(){
        $posts = DB::table('posts')
                                ->orderBy('created_at','desc')
                                ->get();
        // dd($posts);
        // $posts = $posts2->reverse();
        return view ('posts.index',compact('posts'));
    }

    public function destroy($id){
        $query=DB::table('posts')->where('id',$id)->delete();
        return redirect('/posts/create')->with('success','Post Berhasil Dihapus!');
    }

}
