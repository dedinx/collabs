<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class commentController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }
    public function create(){
        
        return view('comments.create');
    }

    public function store(Request $request){
    
        //dd($request->all());
        $comments2 = DB::table('comments')->insert
        ([
            "tulisan" => $request["komentar"],
            "users_id" => Auth::id(),
            "posts_id" => Auth::id()

        ]);
        return redirect('/comments/create');
    }

    public function index(){
        $comments = DB::table('comments')->get();
         //dd($comments);
        // $posts = $posts2->reverse();
        return view ('comments.index',compact('comments'));
    }


}
