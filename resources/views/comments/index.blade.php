@extends('adminlte.master')

@section('content')

    <div class="card ml-3 mr-3 mt-2">
  
  <div class="card-body">
    <div class="tab-content">
      <div class="active tab-pane" id="activity">

        <!-- Post -->
        
        @foreach($comments as $key => $comments)
        <div class="post">
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="{{asset('/adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
            <span class="username">
              <a href="#">{{ Auth::user()->name }}</a>
              
            </span>
            
          </div>
          
          <p>
          {{ $comments -> tulisan }}
          </p>

        </div>
        @endforeach
    
      </div><!-- /.card-body -->
    </div>
  </div>


@endsection