@extends('adminlte.master')

@section('content')

<div class="post ml-3 mt-3 mr-3">
  <div class="user-block">
    <img class="img-circle img-bordered-sm" src="{{asset('/adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
    <span class="username">
      <a href="#">{{ Auth::user()->name }}</a>
      
    </span>
    
  </div>
  <!-- /.user-block -->
  

  <form action="/comments/create" method="POST">
    @csrf
    <div class="input-group input-group-sm">
      <input class="form-control form-control-sm" name="komentar" placeholder="Response">
      <div class="input-group-append">
        <button type="submit" class="btn btn-danger">Send</button>
      </div>
    </div>
  </form>

</div>





@endsection