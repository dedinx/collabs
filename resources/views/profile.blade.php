@extends('adminlte.master')

@section('content')
<div class="container">
  <section class="content">
    @if(session('error'))
    <div class="alert alert-success">
      {{ session('error') }}
    </div>
    @endif
    <div class="container-fluid">
      <div class="row mt-2">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                @if(empty($user->photo_profile))
                <img class="profile-user-img img-fluid img-circle"
                src="{{asset('image/default_profile.jpg')}}"
                alt="User profile picture">
                @else
                <img class="profile-user-img img-fluid img-circle"
                src="{{url('/profile')}}/{{$user->photo_profile}}"
                alt="User profile picture">
                @endif
              </div>

              <h3 class="profile-username text-center">{{$user->name}}</h3>

              <p class="text-muted text-center">{{$user->email}}</p>
              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>Followers</b> <a class="float-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="float-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="float-right">13,287</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <h2 class="btn btn-primary">setting</h2>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <form action="user/update" method="POST" enctype="multipart/form-data" class="form-horizontal">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ $user->id }}"> <br/>
                  <!-- profil upload -->
                  <div class="form-group row">
                    @if (empty($user->photo_profile))
                    <b>Upoad Photo Profile </b><br/>
                    <label for="inputphoto" class="">Upoad Photo Profile</label>
                    <input type="file" name="photo_profile">
                    @else
                    <div class="row">
                      <img class="col-sm-2 profile-user-img img-fluid img-circle"
                      src="{{url('/profile')}}/{{$user->photo_profile}}"
                      alt="User profile picture">
                      <div class="col ml-4 my-auto">
                        <label><b>Upoad Photo Profile baru </b> <i>*jika ingin mengganti profile</i></label>
                        <input type="file" name="photo_profile" value="{{$user->photo_profile}}">
                      </div>
                    </div>
                    @endif
                  </div>
                  <!-- /profil_upload -->
                  <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" value="{{$user->name}}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="{{$user->email}}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputExperience" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputExperience" placeholder="Experience" name="alamat">{{$user->alamat}}</textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
  </div>
  @endsection
