@extends('adminlte.master')

@section('content')

<div class="mr-3 ml-3 mt-5">
    @if(session('success'))
      <div class="alert alert-success">
        {{ session('success') }}
      </div>
    @endif
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Post!!</h3>
    </div>
   
              <!-- /.box-header -->
              <!-- form start -->
    <form role="form" action="/posts" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">  
          <label for="judul">Masukkan Status</label>
          <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul','')}}" placeholder="Apa yang anda pikirkan, {{ Auth::user()->name }} ?"  require>
          
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="mt-3 mb-1">
          <button type="submit" class="btn btn-danger">Quotes</button>
          <button type="submit" class="btn btn-primary">Photo</button>
          </div>
        </div>
        <!-- <div class="form-group">
          <label for="isi">isi</label>
          <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi','')}}" placeholder="Enter isi" require>
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror -->
      </div>    
      <!-- /.box-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-success">Create</button>
      </div>
    </form>

  </div>
</div>

<div class="card ml-3 mr-3 mt-2">
  
  <div class="card-body">
    <div class="tab-content">
      <div class="active tab-pane" id="activity">

        <!-- Post -->
        
        @foreach($posts as $key => $post)
        <div class="post">
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="{{asset('/adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
            <span class="username">
              <a href="#">{{ Auth::user()->name }}</a>

              <form action="/posts/create/{{ $post->id }}" method="post">
              @csrf
              @method('DELETE')
              <input type="submit" value="delete" class="btn-sm btn-danger float-right">
              </form>

              <a href="/posts/{{$post -> id}}/edit" class="btn-sm btn-info float-right">edit</a>

            </span>
            <span class="description">Shared publicly - {{ $post -> created_at }}</span>
          </div>
          <!-- /.user-block -->
          <p>
          {{ $post -> konten_posting }}
          </p>

          <p>
            <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
            <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
            <span class="float-right">
              <a href="{!! url('/comments/create'); !!}" class="link-black text-sm">
                <i class="far fa-comments mr-1"></i> Comments
              </a>
            </span>
          </p>

        </div>
        @endforeach
    
      </div><!-- /.card-body -->
    </div>
  </div>

</div>

@endsection