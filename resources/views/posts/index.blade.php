@extends('adminlte.master')

@section('content')
<div class="card ml-3 mr-3 mt-2">
  
  <div class="card-body">
    <div class="tab-content">
      <div class="active tab-pane" id="activity">
       
        <!-- Post -->

        
        @foreach($posts as $key => $post)
        <div class="post">
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="{{asset('/adminlte/dist/img/user1-128x128.jpg')}}" alt="user image">
            <span class="username">
              <a href="#">{{ $post -> users_id }}</a>
              <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
            </span>
            <span class="description">Shared publicly - {{ $post -> created_at }}</span>
          </div>
          <!-- /.user-block -->
          <p>
            {{ $post -> konten_posting}}
          </p>

          <p>
            <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
            <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
            <span class="float-right">
              <a href="#" class="link-black text-sm">
                <i class="far fa-comments mr-1"></i> Comments (5)
              </a>
            </span>
          </p>

          <div class="input-group input-group-sm mb-0">
            <input class="form-control form-control-sm" placeholder="Response">
            <div class="input-group-append">
              <button type="submit" class="btn btn-danger">Send</button>
            </div>
          </div>

        </div>
        @endforeach
        
    
      </div><!-- /.card-body -->
    </div>
  </div>

</div>
@endsection