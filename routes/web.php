<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');
Route::get('/register', 'Auth\RegisterController@showRegisterForm');
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// ======================================================================
Route::get('/posts/create','PostController@create');
Route::post('/posts','PostController@store');
Route::get('/posts','PostController@index');
Route::delete('/posts/create/{id}','PostController@destroy');

Route::get('/user', 'UserController@user')->name('user');
Route::post('/user/update', 'UserController@update')->name('user_update');



Route::get('/comments/create','commentController@create');
Route::post('/comments/create','commentController@store');
Route::get('/comments','commentController@index');